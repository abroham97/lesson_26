#include <iostream>
#include <math.h>

class  Vector
{
public:
	Vector() : x(7), y(6), z(3)
	{}

	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void Show()
	{
		std::cout << "\n" << x << ' ' << y << ' ' << z << '\n';
	}

	double Length()
	{
		double result = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
		return result;
	}

private:
	double x = 0;
	double y = 0;
	double z = 0;
};

class MyClass
{
private:
	int a;
	int b;
public:
	void SetA(int newA)
	{
		a = newA;
	}

	void SetB(int newB)
	{
		b = newB;
	}

	int Sum()
	{
		int sum = a + b;
		return sum;
	}
};

int main()
{
	int value1 = 9;
	int value2 = 4;

	MyClass myObj;
	myObj.SetA(value1);
	myObj.SetB(value2);
	int sum = myObj.Sum();
	std::cout << value1 << " + " << value2 << " = " << sum << "\n";

	double x = 9;
	double y = 8;
	double z = 4;

	Vector vector(x, y, z);
	double length = vector.Length();
	std::cout << "Length of vector (" << x << ", " << y << ", " << z << ") is " << length << "\n";
}
